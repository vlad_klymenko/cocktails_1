﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace cocktails
{
    public partial class Global_form : Form
    {
        public Global_form()
        {
            InitializeComponent();
        }

        private void storekeeper_button_Click(object sender, EventArgs e)
        {
            storekeeper_form storekeeperform = new storekeeper_form();
            storekeeperform.ShowDialog();
        }

        private void barmen_button_Click(object sender, EventArgs e)
        {
            barmen_form barmenform = new barmen_form();
            barmenform.ShowDialog();
        }
    }
}

﻿namespace cocktails
{
    partial class barmen_form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.components = new System.ComponentModel.Container();
			this.recipesBindingSource = new System.Windows.Forms.BindingSource(this.components);
			this.database_cocktailsDataSetRecipes = new cocktails.Database_cocktailsDataSetRecipes();
			this.recipesTableAdapter = new cocktails.Database_cocktailsDataSetRecipesTableAdapters.recipesTableAdapter();
			this.order_processing_button = new System.Windows.Forms.Button();
			this.editing_cocktails_button = new System.Windows.Forms.Button();
			((System.ComponentModel.ISupportInitialize)(this.recipesBindingSource)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.database_cocktailsDataSetRecipes)).BeginInit();
			this.SuspendLayout();
			// 
			// recipesBindingSource
			// 
			this.recipesBindingSource.DataMember = "recipes";
			this.recipesBindingSource.DataSource = this.database_cocktailsDataSetRecipes;
			// 
			// database_cocktailsDataSetRecipes
			// 
			this.database_cocktailsDataSetRecipes.DataSetName = "Database_cocktailsDataSetRecipes";
			this.database_cocktailsDataSetRecipes.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
			// 
			// recipesTableAdapter
			// 
			this.recipesTableAdapter.ClearBeforeFill = true;
			// 
			// order_processing_button
			// 
			this.order_processing_button.BackColor = System.Drawing.SystemColors.ControlLightLight;
			this.order_processing_button.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
			this.order_processing_button.Location = new System.Drawing.Point(12, 12);
			this.order_processing_button.Name = "order_processing_button";
			this.order_processing_button.Size = new System.Drawing.Size(138, 74);
			this.order_processing_button.TabIndex = 0;
			this.order_processing_button.Text = "Обработка заказов";
			this.order_processing_button.UseVisualStyleBackColor = false;
			this.order_processing_button.Click += new System.EventHandler(this.order_processing_button_Click);
			// 
			// editing_cocktails_button
			// 
			this.editing_cocktails_button.BackColor = System.Drawing.SystemColors.ControlLightLight;
			this.editing_cocktails_button.Location = new System.Drawing.Point(156, 12);
			this.editing_cocktails_button.Name = "editing_cocktails_button";
			this.editing_cocktails_button.Size = new System.Drawing.Size(144, 74);
			this.editing_cocktails_button.TabIndex = 1;
			this.editing_cocktails_button.Text = "Редактирование коктейлей";
			this.editing_cocktails_button.UseVisualStyleBackColor = false;
			this.editing_cocktails_button.Click += new System.EventHandler(this.editing_cocktails_button_Click);
			// 
			// barmen_form
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(314, 99);
			this.Controls.Add(this.editing_cocktails_button);
			this.Controls.Add(this.order_processing_button);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.MaximizeBox = false;
			this.Name = "barmen_form";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Бармен";
			this.Load += new System.EventHandler(this.barmen_form_Load);
			((System.ComponentModel.ISupportInitialize)(this.recipesBindingSource)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.database_cocktailsDataSetRecipes)).EndInit();
			this.ResumeLayout(false);

        }

        #endregion

        private Database_cocktailsDataSetRecipes database_cocktailsDataSetRecipes;
        private System.Windows.Forms.BindingSource recipesBindingSource;
        private Database_cocktailsDataSetRecipesTableAdapters.recipesTableAdapter recipesTableAdapter;
        private System.Windows.Forms.Button order_processing_button;
        private System.Windows.Forms.Button editing_cocktails_button;
    }
}
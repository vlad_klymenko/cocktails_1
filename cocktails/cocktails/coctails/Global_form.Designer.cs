﻿namespace cocktails
{
    partial class Global_form
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
			this.storekeeper_button = new System.Windows.Forms.Button();
			this.barmen_button = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// storekeeper_button
			// 
			this.storekeeper_button.BackColor = System.Drawing.SystemColors.ControlLightLight;
			this.storekeeper_button.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.storekeeper_button.Location = new System.Drawing.Point(12, 12);
			this.storekeeper_button.Name = "storekeeper_button";
			this.storekeeper_button.Size = new System.Drawing.Size(234, 99);
			this.storekeeper_button.TabIndex = 0;
			this.storekeeper_button.Text = "Владелец склада";
			this.storekeeper_button.UseVisualStyleBackColor = false;
			this.storekeeper_button.Click += new System.EventHandler(this.storekeeper_button_Click);
			// 
			// barmen_button
			// 
			this.barmen_button.BackColor = System.Drawing.SystemColors.ControlLightLight;
			this.barmen_button.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.barmen_button.Location = new System.Drawing.Point(12, 117);
			this.barmen_button.Name = "barmen_button";
			this.barmen_button.Size = new System.Drawing.Size(234, 98);
			this.barmen_button.TabIndex = 1;
			this.barmen_button.Text = "Бармен";
			this.barmen_button.UseVisualStyleBackColor = false;
			this.barmen_button.Click += new System.EventHandler(this.barmen_button_Click);
			// 
			// Global_form
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(258, 227);
			this.Controls.Add(this.barmen_button);
			this.Controls.Add(this.storekeeper_button);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.MaximizeBox = false;
			this.Name = "Global_form";
			this.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Главное окно";
			this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button storekeeper_button;
        private System.Windows.Forms.Button barmen_button;
    }
}


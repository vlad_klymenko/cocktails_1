﻿namespace cocktails
{
    partial class editing_cocktails_Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.components = new System.ComponentModel.Container();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
			this.dataGridView1 = new System.Windows.Forms.DataGridView();
			this.idDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.nameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.edit_recipe = new System.Windows.Forms.DataGridViewButtonColumn();
			this.recipeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.priceDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.recipesBindingSource = new System.Windows.Forms.BindingSource(this.components);
			this.database_cocktailsDataSet1 = new cocktails.Database_cocktailsDataSet1();
			this.recipesTableAdapter = new cocktails.Database_cocktailsDataSet1TableAdapters.recipesTableAdapter();
			this.accept_editing_button = new System.Windows.Forms.Button();
			((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.recipesBindingSource)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.database_cocktailsDataSet1)).BeginInit();
			this.SuspendLayout();
			// 
			// dataGridView1
			// 
			this.dataGridView1.AutoGenerateColumns = false;
			this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idDataGridViewTextBoxColumn,
            this.nameDataGridViewTextBoxColumn,
            this.edit_recipe,
            this.recipeDataGridViewTextBoxColumn,
            this.priceDataGridViewTextBoxColumn});
			this.dataGridView1.DataSource = this.recipesBindingSource;
			this.dataGridView1.Location = new System.Drawing.Point(12, 12);
			this.dataGridView1.Name = "dataGridView1";
			this.dataGridView1.Size = new System.Drawing.Size(500, 208);
			this.dataGridView1.TabIndex = 0;
			this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
			// 
			// idDataGridViewTextBoxColumn
			// 
			this.idDataGridViewTextBoxColumn.DataPropertyName = "Id";
			this.idDataGridViewTextBoxColumn.HeaderText = "Id";
			this.idDataGridViewTextBoxColumn.Name = "idDataGridViewTextBoxColumn";
			this.idDataGridViewTextBoxColumn.ReadOnly = true;
			this.idDataGridViewTextBoxColumn.Visible = false;
			// 
			// nameDataGridViewTextBoxColumn
			// 
			this.nameDataGridViewTextBoxColumn.DataPropertyName = "name";
			this.nameDataGridViewTextBoxColumn.HeaderText = "Название";
			this.nameDataGridViewTextBoxColumn.Name = "nameDataGridViewTextBoxColumn";
			// 
			// edit_recipe
			// 
			dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
			dataGridViewCellStyle2.NullValue = "Редактировать";
			this.edit_recipe.DefaultCellStyle = dataGridViewCellStyle2;
			this.edit_recipe.HeaderText = "Изменить рецепт";
			this.edit_recipe.Name = "edit_recipe";
			this.edit_recipe.ReadOnly = true;
			this.edit_recipe.Width = 160;
			// 
			// recipeDataGridViewTextBoxColumn
			// 
			this.recipeDataGridViewTextBoxColumn.DataPropertyName = "recipe";
			this.recipeDataGridViewTextBoxColumn.HeaderText = "Рецепт";
			this.recipeDataGridViewTextBoxColumn.Name = "recipeDataGridViewTextBoxColumn";
			this.recipeDataGridViewTextBoxColumn.Visible = false;
			// 
			// priceDataGridViewTextBoxColumn
			// 
			this.priceDataGridViewTextBoxColumn.DataPropertyName = "price";
			this.priceDataGridViewTextBoxColumn.HeaderText = "Цена";
			this.priceDataGridViewTextBoxColumn.Name = "priceDataGridViewTextBoxColumn";
			// 
			// recipesBindingSource
			// 
			this.recipesBindingSource.DataMember = "recipes";
			this.recipesBindingSource.DataSource = this.database_cocktailsDataSet1;
			// 
			// database_cocktailsDataSet1
			// 
			this.database_cocktailsDataSet1.DataSetName = "Database_cocktailsDataSet1";
			this.database_cocktailsDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
			// 
			// recipesTableAdapter
			// 
			this.recipesTableAdapter.ClearBeforeFill = true;
			// 
			// accept_editing_button
			// 
			this.accept_editing_button.BackColor = System.Drawing.SystemColors.ControlLightLight;
			this.accept_editing_button.Location = new System.Drawing.Point(12, 227);
			this.accept_editing_button.Name = "accept_editing_button";
			this.accept_editing_button.Size = new System.Drawing.Size(500, 39);
			this.accept_editing_button.TabIndex = 1;
			this.accept_editing_button.Text = "Обновить";
			this.accept_editing_button.UseVisualStyleBackColor = false;
			this.accept_editing_button.Click += new System.EventHandler(this.accept_editing_button_Click);
			// 
			// editing_cocktails_Form
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(546, 280);
			this.Controls.Add(this.accept_editing_button);
			this.Controls.Add(this.dataGridView1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.MaximizeBox = false;
			this.Name = "editing_cocktails_Form";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Редактирование коктейлей";
			this.Load += new System.EventHandler(this.editing_cocktails_Form_Load);
			((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.recipesBindingSource)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.database_cocktailsDataSet1)).EndInit();
			this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private Database_cocktailsDataSet1 database_cocktailsDataSet1;
        private System.Windows.Forms.BindingSource recipesBindingSource;
        private Database_cocktailsDataSet1TableAdapters.recipesTableAdapter recipesTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewButtonColumn edit_recipe;
        private System.Windows.Forms.DataGridViewTextBoxColumn recipeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn priceDataGridViewTextBoxColumn;
        private System.Windows.Forms.Button accept_editing_button;
    }
}
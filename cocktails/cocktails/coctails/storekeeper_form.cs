﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace cocktails
{
    public partial class storekeeper_form : Form
    {
        public storekeeper_form()
        {
            InitializeComponent();
        }

        private void storekeeper_form_Load(object sender, EventArgs e)
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "database_cocktailsDataSetTare.tare". При необходимости она может быть перемещена или удалена.
            this.tareTableAdapter.Fill(this.database_cocktailsDataSetTare.tare);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "database_cocktailsDataSetProduct.products". При необходимости она может быть перемещена или удалена.
            this.productsTableAdapter.Fill(this.database_cocktailsDataSetProduct.products);
           

        }

        private void button_save_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.productsBindingSource.EndEdit();
            this.productsTableAdapter.Update(this.database_cocktailsDataSetProduct);
            this.tareBindingSource.EndEdit();
            this.tareTableAdapter.Update(this.database_cocktailsDataSetTare);
        }
    }
}

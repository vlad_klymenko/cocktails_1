﻿namespace cocktails
{
    partial class recipes_form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.components = new System.ComponentModel.Container();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
			this.dataGridViewProducts = new System.Windows.Forms.DataGridView();
			this.idDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.nameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.priceDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.typeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.quantityDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.productsBindingSource = new System.Windows.Forms.BindingSource(this.components);
			this.database_cocktailsDataSet2 = new cocktails.Database_cocktailsDataSet2();
			this.dataGridViewTare = new System.Windows.Forms.DataGridView();
			this.idDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.typeDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.priceDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.quantityDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.timeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.tareBindingSource = new System.Windows.Forms.BindingSource(this.components);
			this.databasecocktailsDataSetTareBindingSource = new System.Windows.Forms.BindingSource(this.components);
			this.database_cocktailsDataSetTare = new cocktails.Database_cocktailsDataSetTare();
			this.productsTableAdapter = new cocktails.Database_cocktailsDataSet2TableAdapters.productsTableAdapter();
			this.tareTableAdapter = new cocktails.Database_cocktailsDataSetTareTableAdapters.tareTableAdapter();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.Продукты = new System.Windows.Forms.GroupBox();
			this.accept_button = new System.Windows.Forms.Button();
			((System.ComponentModel.ISupportInitialize)(this.dataGridViewProducts)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.productsBindingSource)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.database_cocktailsDataSet2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.dataGridViewTare)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.tareBindingSource)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.databasecocktailsDataSetTareBindingSource)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.database_cocktailsDataSetTare)).BeginInit();
			this.groupBox2.SuspendLayout();
			this.Продукты.SuspendLayout();
			this.SuspendLayout();
			// 
			// dataGridViewProducts
			// 
			this.dataGridViewProducts.AllowUserToAddRows = false;
			this.dataGridViewProducts.AllowUserToDeleteRows = false;
			this.dataGridViewProducts.AutoGenerateColumns = false;
			this.dataGridViewProducts.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dataGridViewProducts.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idDataGridViewTextBoxColumn,
            this.nameDataGridViewTextBoxColumn,
            this.priceDataGridViewTextBoxColumn,
            this.typeDataGridViewTextBoxColumn,
            this.quantityDataGridViewTextBoxColumn});
			this.dataGridViewProducts.DataSource = this.productsBindingSource;
			this.dataGridViewProducts.Dock = System.Windows.Forms.DockStyle.Fill;
			this.dataGridViewProducts.Location = new System.Drawing.Point(3, 16);
			this.dataGridViewProducts.Name = "dataGridViewProducts";
			this.dataGridViewProducts.Size = new System.Drawing.Size(464, 121);
			this.dataGridViewProducts.TabIndex = 0;
			// 
			// idDataGridViewTextBoxColumn
			// 
			this.idDataGridViewTextBoxColumn.DataPropertyName = "Id";
			this.idDataGridViewTextBoxColumn.HeaderText = "Id";
			this.idDataGridViewTextBoxColumn.Name = "idDataGridViewTextBoxColumn";
			this.idDataGridViewTextBoxColumn.ReadOnly = true;
			this.idDataGridViewTextBoxColumn.Visible = false;
			// 
			// nameDataGridViewTextBoxColumn
			// 
			this.nameDataGridViewTextBoxColumn.DataPropertyName = "name";
			this.nameDataGridViewTextBoxColumn.HeaderText = "Название";
			this.nameDataGridViewTextBoxColumn.Name = "nameDataGridViewTextBoxColumn";
			// 
			// priceDataGridViewTextBoxColumn
			// 
			this.priceDataGridViewTextBoxColumn.DataPropertyName = "price";
			this.priceDataGridViewTextBoxColumn.HeaderText = "Цена";
			this.priceDataGridViewTextBoxColumn.Name = "priceDataGridViewTextBoxColumn";
			// 
			// typeDataGridViewTextBoxColumn
			// 
			this.typeDataGridViewTextBoxColumn.DataPropertyName = "type";
			this.typeDataGridViewTextBoxColumn.HeaderText = "Тип";
			this.typeDataGridViewTextBoxColumn.Name = "typeDataGridViewTextBoxColumn";
			// 
			// quantityDataGridViewTextBoxColumn
			// 
			dataGridViewCellStyle7.Format = "N2";
			dataGridViewCellStyle7.NullValue = "0";
			this.quantityDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle7;
			this.quantityDataGridViewTextBoxColumn.HeaderText = "Количество";
			this.quantityDataGridViewTextBoxColumn.Name = "quantityDataGridViewTextBoxColumn";
			// 
			// productsBindingSource
			// 
			this.productsBindingSource.DataMember = "products";
			this.productsBindingSource.DataSource = this.database_cocktailsDataSet2;
			// 
			// database_cocktailsDataSet2
			// 
			this.database_cocktailsDataSet2.DataSetName = "Database_cocktailsDataSet2";
			this.database_cocktailsDataSet2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
			// 
			// dataGridViewTare
			// 
			this.dataGridViewTare.AllowUserToAddRows = false;
			this.dataGridViewTare.AllowUserToDeleteRows = false;
			this.dataGridViewTare.AutoGenerateColumns = false;
			this.dataGridViewTare.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dataGridViewTare.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idDataGridViewTextBoxColumn1,
            this.typeDataGridViewTextBoxColumn1,
            this.priceDataGridViewTextBoxColumn1,
            this.quantityDataGridViewTextBoxColumn1,
            this.timeDataGridViewTextBoxColumn});
			this.dataGridViewTare.DataSource = this.tareBindingSource;
			this.dataGridViewTare.Dock = System.Windows.Forms.DockStyle.Fill;
			this.dataGridViewTare.Location = new System.Drawing.Point(3, 16);
			this.dataGridViewTare.Name = "dataGridViewTare";
			this.dataGridViewTare.Size = new System.Drawing.Size(461, 111);
			this.dataGridViewTare.TabIndex = 1;
			// 
			// idDataGridViewTextBoxColumn1
			// 
			this.idDataGridViewTextBoxColumn1.DataPropertyName = "Id";
			this.idDataGridViewTextBoxColumn1.HeaderText = "Id";
			this.idDataGridViewTextBoxColumn1.Name = "idDataGridViewTextBoxColumn1";
			this.idDataGridViewTextBoxColumn1.ReadOnly = true;
			this.idDataGridViewTextBoxColumn1.Visible = false;
			// 
			// typeDataGridViewTextBoxColumn1
			// 
			this.typeDataGridViewTextBoxColumn1.DataPropertyName = "type";
			this.typeDataGridViewTextBoxColumn1.HeaderText = "Тип";
			this.typeDataGridViewTextBoxColumn1.Name = "typeDataGridViewTextBoxColumn1";
			// 
			// priceDataGridViewTextBoxColumn1
			// 
			this.priceDataGridViewTextBoxColumn1.DataPropertyName = "price";
			this.priceDataGridViewTextBoxColumn1.HeaderText = "Цена";
			this.priceDataGridViewTextBoxColumn1.Name = "priceDataGridViewTextBoxColumn1";
			// 
			// quantityDataGridViewTextBoxColumn1
			// 
			dataGridViewCellStyle8.Format = "N0";
			dataGridViewCellStyle8.NullValue = "0";
			this.quantityDataGridViewTextBoxColumn1.DefaultCellStyle = dataGridViewCellStyle8;
			this.quantityDataGridViewTextBoxColumn1.HeaderText = "Количество";
			this.quantityDataGridViewTextBoxColumn1.Name = "quantityDataGridViewTextBoxColumn1";
			// 
			// timeDataGridViewTextBoxColumn
			// 
			this.timeDataGridViewTextBoxColumn.DataPropertyName = "time";
			this.timeDataGridViewTextBoxColumn.HeaderText = "Время";
			this.timeDataGridViewTextBoxColumn.Name = "timeDataGridViewTextBoxColumn";
			this.timeDataGridViewTextBoxColumn.Visible = false;
			// 
			// tareBindingSource
			// 
			this.tareBindingSource.DataMember = "tare";
			this.tareBindingSource.DataSource = this.databasecocktailsDataSetTareBindingSource;
			// 
			// databasecocktailsDataSetTareBindingSource
			// 
			this.databasecocktailsDataSetTareBindingSource.DataSource = this.database_cocktailsDataSetTare;
			this.databasecocktailsDataSetTareBindingSource.Position = 0;
			// 
			// database_cocktailsDataSetTare
			// 
			this.database_cocktailsDataSetTare.DataSetName = "Database_cocktailsDataSetTare";
			this.database_cocktailsDataSetTare.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
			// 
			// productsTableAdapter
			// 
			this.productsTableAdapter.ClearBeforeFill = true;
			// 
			// tareTableAdapter
			// 
			this.tareTableAdapter.ClearBeforeFill = true;
			// 
			// groupBox2
			// 
			this.groupBox2.Controls.Add(this.dataGridViewTare);
			this.groupBox2.Location = new System.Drawing.Point(15, 169);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(467, 130);
			this.groupBox2.TabIndex = 4;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "Тара";
			// 
			// Продукты
			// 
			this.Продукты.Controls.Add(this.dataGridViewProducts);
			this.Продукты.Location = new System.Drawing.Point(12, 23);
			this.Продукты.Name = "Продукты";
			this.Продукты.Size = new System.Drawing.Size(470, 140);
			this.Продукты.TabIndex = 5;
			this.Продукты.TabStop = false;
			this.Продукты.Text = "Продукты";
			// 
			// accept_button
			// 
			this.accept_button.Location = new System.Drawing.Point(18, 305);
			this.accept_button.Name = "accept_button";
			this.accept_button.Size = new System.Drawing.Size(464, 43);
			this.accept_button.TabIndex = 6;
			this.accept_button.Text = "Изменить";
			this.accept_button.UseVisualStyleBackColor = true;
			this.accept_button.Click += new System.EventHandler(this.accept_button_Click);
			// 
			// recipes_form
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(489, 360);
			this.Controls.Add(this.accept_button);
			this.Controls.Add(this.Продукты);
			this.Controls.Add(this.groupBox2);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.MaximizeBox = false;
			this.Name = "recipes_form";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Рецепты";
			this.Load += new System.EventHandler(this.recipes_form_Load);
			((System.ComponentModel.ISupportInitialize)(this.dataGridViewProducts)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.productsBindingSource)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.database_cocktailsDataSet2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.dataGridViewTare)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.tareBindingSource)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.databasecocktailsDataSetTareBindingSource)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.database_cocktailsDataSetTare)).EndInit();
			this.groupBox2.ResumeLayout(false);
			this.Продукты.ResumeLayout(false);
			this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridViewProducts;
        private System.Windows.Forms.DataGridView dataGridViewTare;
        private Database_cocktailsDataSet2 database_cocktailsDataSet2;
        private System.Windows.Forms.BindingSource productsBindingSource;
        private Database_cocktailsDataSet2TableAdapters.productsTableAdapter productsTableAdapter;
        private System.Windows.Forms.BindingSource databasecocktailsDataSetTareBindingSource;
        private Database_cocktailsDataSetTare database_cocktailsDataSetTare;
        private System.Windows.Forms.BindingSource tareBindingSource;
        private Database_cocktailsDataSetTareTableAdapters.tareTableAdapter tareTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn typeDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn priceDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn quantityDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn timeDataGridViewTextBoxColumn;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox Продукты;
        private System.Windows.Forms.Button accept_button;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn priceDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn typeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn quantityDataGridViewTextBoxColumn;
    }
}
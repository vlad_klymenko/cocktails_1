﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace cocktails
{
    public partial class barmen_form : Form
    {
        public barmen_form()
        {
            InitializeComponent();
        }

        private void barmen_form_Load(object sender, EventArgs e)
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "database_cocktailsDataSetRecipes.recipes". При необходимости она может быть перемещена или удалена.
            this.recipesTableAdapter.Fill(this.database_cocktailsDataSetRecipes.recipes);

        }

        private void order_processing_button_Click(object sender, EventArgs e)
        {
            order_processing_Form order_processing = new order_processing_Form();
            order_processing.ShowDialog();
        }

        private void editing_cocktails_button_Click(object sender, EventArgs e)
        {
            editing_cocktails_Form editing_cocktails = new editing_cocktails_Form();
            editing_cocktails.ShowDialog();
        }
    }
}

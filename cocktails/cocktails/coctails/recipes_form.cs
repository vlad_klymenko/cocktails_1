﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace cocktails
{
    public partial class recipes_form : Form
    {

        string result = string.Empty;
        string recipeText = string.Empty;
        public recipes_form(string recipeText)
        {
            InitializeComponent();
            this.recipeText = recipeText;
        }

        private void recipes_form_Load(object sender, EventArgs e)
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "database_cocktailsDataSetTare.tare". При необходимости она может быть перемещена или удалена.
            this.tareTableAdapter.Fill(this.database_cocktailsDataSetTare.tare);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "database_cocktailsDataSet2.products". При необходимости она может быть перемещена или удалена.
            this.productsTableAdapter.Fill(this.database_cocktailsDataSet2.products);
            if (recipeText != String.Empty)
            {


                string[] mas = recipeText.Split(new char[] { '&' });
                
                foreach (string product in mas[0].Split(new char[] { ';' }))
                {
                    string[] info = product.Split(new char[] { ':' });
                    int counter = 0;
                    foreach (DataGridViewRow row in dataGridViewProducts.Rows)
                    {
                        if ((int)row.Cells[idDataGridViewTextBoxColumn.Name].Value == int.Parse(info[0]))
                        {

                            dataGridViewProducts.Rows[counter].Cells[quantityDataGridViewTextBoxColumn.Name].Value = decimal.Parse(info[1]);
                            
                        }
                        counter++;
                    }
                }
                foreach (string tare in mas[1].Split(new char[] { ';' }))
                {
                    string[] info = tare.Split(new char[] { ':' });
                    int counter = 0;
                    foreach (DataGridViewRow row in dataGridViewTare.Rows)
                    {
                        if ((int)row.Cells[idDataGridViewTextBoxColumn1.Name].Value == int.Parse(info[0]))
                        {
                            dataGridViewTare.Rows[counter].Cells[quantityDataGridViewTextBoxColumn1.Name].Value = decimal.Parse(info[1]);
                        }
                        counter++;
                    }
                }
            }
        }
        public string GetResult()
        {
            return result;
        }

        private void accept_button_Click(object sender, EventArgs e)
        {
            string products = string.Empty;
            string tare = string.Empty;
            foreach (DataGridViewRow row in dataGridViewProducts.Rows)
            {
                if (row.Cells[quantityDataGridViewTextBoxColumn.Name].Value != null)
                {
                    products += row.Cells[idDataGridViewTextBoxColumn.Name].Value.ToString()
                        + ":" + row.Cells[quantityDataGridViewTextBoxColumn.Name].Value.ToString() + ";";
                }
            }
			if(products != string.Empty)
			{
				products = products.Substring(0, products.Length - 1);
			}
            foreach (DataGridViewRow row in dataGridViewTare.Rows)
            {
                if (row.Cells[quantityDataGridViewTextBoxColumn1.Name].Value != null)
                {
                    tare += row.Cells[idDataGridViewTextBoxColumn1.Name].Value.ToString()
                        + ":" + row.Cells[quantityDataGridViewTextBoxColumn1.Name].Value.ToString() + ";";
                }
            }
			if(tare != string.Empty)
			{
				tare = tare.Substring(0, tare.Length - 1);
            }
			if (tare != string.Empty && products != string.Empty)
			{
				result = products + "&" + tare;
				DialogResult = System.Windows.Forms.DialogResult.OK;
			}
			else
			{
				MessageBox.Show("Не все елементы выбраны!", "Предупреждение");
			}
        }
    }
}
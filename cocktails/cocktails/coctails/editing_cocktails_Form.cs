﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace cocktails
{
    public partial class editing_cocktails_Form : Form
    {
        public editing_cocktails_Form()
        {
            InitializeComponent();
        }

        private void editing_cocktails_Form_Load(object sender, EventArgs e)
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "database_cocktailsDataSet1.recipes". При необходимости она может быть перемещена или удалена.
            this.recipesTableAdapter.Fill(this.database_cocktailsDataSet1.recipes);

        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == edit_recipe.Index )
            {
                recipes_form Recipes_Form = new recipes_form(
                    dataGridView1[recipeDataGridViewTextBoxColumn.Name, e.RowIndex].Value.ToString()
                        );
                Recipes_Form.ShowDialog();
                if (Recipes_Form.DialogResult == System.Windows.Forms.DialogResult.OK)
                {
                    dataGridView1[recipeDataGridViewTextBoxColumn.Name, e.RowIndex].Value =
                        Recipes_Form.GetResult();
                }
            }
        }

        private void accept_editing_button_Click(object sender, EventArgs e)
        {
            try
            {

                this.Validate();
                this.recipesBindingSource.EndEdit();
                this.recipesTableAdapter.Update(this.database_cocktailsDataSet1);
                this.recipesTableAdapter.Fill(this.database_cocktailsDataSet1.recipes);
                MessageBox.Show("Обновление успешно");
            }
            catch (System.Exception ex)
            {
                MessageBox.Show("Ошибка обновления");
            }

        }

    }

}
